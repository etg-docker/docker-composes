# prepare plugin

PLUGINNAME=download-monitor
DOWNLOAD=https://downloads.wordpress.org/plugin/$PLUGINNAME.latest-stable.zip

echo "prepare plugin files: download and unzip"
echo

echo "downloading $DOWNLOAD"
echo

curl --output $PLUGINNAME.zip $DOWNLOAD

echo
echo "Download complete"

echo
echo "unzipping $PLUGINNAME.zip"
echo

unzip -q $PLUGINNAME.zip

echo
echo "removing $PLUGINNAME.zip"
echo

rm $PLUGINNAME.zip
