#!/bin/bash

echo "Build image ekleinod/graphql-php"
echo

docker build --tag ekleinod/graphql-php --build-arg PHP_VERSION=8 .
