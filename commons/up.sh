# start docker compose as daemon

docker-compose up -d

echo
echo "started containers"
echo
echo "Check liveliness with"
echo "docker container ls -a"
echo
echo "wordpress: http://localhost:8000/"
